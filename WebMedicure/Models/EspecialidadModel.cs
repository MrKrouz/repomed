﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMedicure.Models
{
    public class EspecialidadModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}