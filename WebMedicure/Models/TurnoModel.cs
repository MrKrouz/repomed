﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebMedicure.Models;

namespace WebMedicure.Models
{
    public class TurnoModel
    {
        public int Id { get; set; }

        public PacienteModel Paciente { get; set; }

        [Required(ErrorMessage = "Por favor seleccione un profesional")]
        public ProfesionalModel Profesional { get; set; }

        [Required(ErrorMessage = "Por favor seleccione una especialidad")]
        public EspecialidadModel Especialidad { get; set; }

        [Required(ErrorMessage = "Por favor seleccione un dia")]
        public DateTime Dia { get; set; }

        public TimeSpan Hora { get; set; }

        public List<ProfesionalModel> Profesionales { get; set; } = new List<ProfesionalModel>();

        public List<EspecialidadModel> Especialidades { get; set; } = new List<EspecialidadModel>();

        public List<DateTime> FechasDeshabilitadas { get; set; } = new List<DateTime>();

        public List<TurnoModel> Turnos { get; set; }
    }
}