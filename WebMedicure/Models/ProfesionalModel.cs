﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebMedicure.Models
{
    public class ProfesionalModel
    {
        [Required(ErrorMessage = "Por favor debe ingresar un v")]
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public EspecialidadModel Especialidad { get; set; }

    }
}