﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMedicure.Models
{
    public class PacienteModel
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Dni { get; set; }
        public string ObraSocial { get; set; }
        public int NumeroAfiliado { get; set; }
        public List<string> ObrasSociales { get; set; }



    }
}