using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using WebMedicure.Models;

[assembly: OwinStartup(typeof(WebMedicure.Startup))]

namespace WebMedicure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesAndUsers();
        }

        private void createRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "matiasdm";
                user.Email = "mmedeot1@gmail.com";

                string userPWD = "Juiced_22354";

                var chkUser = userManager.Create(user, userPWD);

                if (chkUser.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Admin");
                }
            }

            if (!roleManager.RoleExists("Medico"))
            {
                var role = new IdentityRole();
                role.Name = "Medico";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Recepcionista"))
            {
                var role = new IdentityRole();
                role.Name = "Recepcionista";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Paciente"))
            {
                var role = new IdentityRole();
                role.Name = "Paciente";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Director"))
            {
                var role = new IdentityRole();
                role.Name = "Director";
                roleManager.Create(role);
            }
        }
    }
}