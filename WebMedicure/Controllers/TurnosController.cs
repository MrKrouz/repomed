﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebMedicure.Models;
using Microsoft.AspNet.Identity;

namespace WebMedicure.Controllers
{
    //[Authorize(Roles = "Paciente"]
    public class TurnosController : Controller
    {
        // GET: Turnos
        public ActionResult Turnos()
        {
            if (User.Identity.IsAuthenticated)
            {
                var model = new TurnoModel();
                var bllPaciente = new BLL.Paciente();
                var bllProfesionales = new BLL.Medico();
                var bllUtilidades = new BLL.Utilidades();
                var listaEspecialides = new List<string>();
                string userId = User.Identity.GetUserId();
                var user = (new ApplicationDbContext()).Users.FirstOrDefault(s => s.Id == userId);

                BE.Paciente paciente = new BE.Paciente();
                paciente = bllPaciente.GetByUserName(user.UserName);
                model.Paciente = new PacienteModel { Nombre = paciente.Nombre, Apellido = paciente.Apellido, Dni = paciente.Dni };

                listaEspecialides = bllUtilidades.GetEspecialidades().ToList();

                for (int i = 0; i < listaEspecialides.Count(); i++)
                {
                    model.Especialidades.Add(new EspecialidadModel { Id = i, Nombre = listaEspecialides[i] });
                }

                foreach (var item in bllProfesionales.GetMedicos())
                {
                    model.Profesionales.Add(new ProfesionalModel { Nombre = item.Nombre, Apellido = item.Apellido, Id = item.Id, Especialidad = model.Especialidades.Where(e => e.Nombre == item.Especialidad.ToString()).FirstOrDefault() });
                }

                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ContentResult GetDias(int variable)
        {
            var bll = new BLL.Turno();
            List<DateTime> lista = new List<DateTime>();
            for (var m = 12; m < 13; m++)
            {
                for (var i = 1; i < 31; i++)
                {
                    if (Convert.ToInt32(new DateTime(2018, m, i).DayOfWeek) == 0 || Convert.ToInt32(new DateTime(2018, m, i).DayOfWeek) == 1)
                    {
                        lista.Add(new DateTime(2018, m, i).Date);
                    }
                }
            }
            foreach (var dia in bll.GetTurnos().Where(t => t.Profesional.Id == variable))
            {
                lista.Add(dia.Fecha.Date);
            }
            return new ContentResult() { Content = JsonConvert.SerializeObject(lista), ContentType = "application/json" };
        }

        [HttpPost]
        public ContentResult GetHoras(DateTime variable, int variable2)
        {
            var bll = new BLL.Turno();
            var listaTurnos = new List<BE.Turno>();
            List<TimeSpan> lista = new List<TimeSpan>();
            listaTurnos = bll.GetTurnos();
            var tiempo = new TimeSpan(12, 00, 00);
            while (tiempo.Hours < 18)
            {
                if (listaTurnos.Where(t => t.Fecha.TimeOfDay == tiempo && t.Profesional.Id == variable2).FirstOrDefault() != null)
                {
                    lista.Add(tiempo);
                }
                else
                {
                    tiempo.Add(new TimeSpan(00, 30, 00));
                }
            }

            return new ContentResult() { Content = JsonConvert.SerializeObject(lista), ContentType = "application/json" };
        }

        [HttpPost]
        public ActionResult Reserva(TurnoModel modelo)
        {
            modelo.Dia = modelo.Dia + modelo.Hora;
            var bll = new BLL.Turno();
            var bllP = new BLL.Paciente();
            string userId = User.Identity.GetUserId();
            var user = (new ApplicationDbContext()).Users.FirstOrDefault(s => s.Id == userId);

            BE.Paciente paciente = new BE.Paciente();
            paciente = bllP.GetByUserName(user.UserName);
            bll.Add(new BE.Turno { Fecha = modelo.Dia, Profesional = new BE.Medico { Id = modelo.Profesional.Id }, Paciente = paciente });
            return RedirectToAction("Index", "Home");
        }
    }
}