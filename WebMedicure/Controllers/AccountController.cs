using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMedicure.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;

namespace WebMedicure.Controllers
{
    public class AccountController : Controller
    {

        ApplicationDbContext context;
        public AccountController()
        {
            context = new ApplicationDbContext();
        }
        ApplicationSignInManager _signInManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                {
                    _signInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
                }
                return _signInManager;
            }
        }

        ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                return _userManager;
            }
        }

        [HttpGet]
        public ActionResult Turnos()
        {
            var model = new TurnoModel();
            var bllTurnos = new BLL.Turno();
            var bllPaciente = new BLL.Paciente();
            string userId = User.Identity.GetUserId();
            var user = (new ApplicationDbContext()).Users.FirstOrDefault(s => s.Id == userId);
            var paciente = new BE.Paciente();
            paciente = bllPaciente.GetByUserName(user.UserName);
            foreach (var item in bllTurnos.GetTurnos().Where(p=> p.Paciente.Id == paciente.Id))
            {
                model.Turnos.Add(new TurnoModel { Id = item.Id, Dia = item.Fecha.Date, Hora = item.Fecha.TimeOfDay, Especialidad = new EspecialidadModel { Nombre = item.Profesional.Especialidad.ToString() }, Profesional = new ProfesionalModel { Nombre = item.Profesional.Nombre, Apellido = item.Profesional.Apellido } });
            }
            return View(model);
        }

        [HttpDelete]
        public void Delete(FormDataCollection form)
        {
            var key = Convert.ToInt32(form.Get("Id"));

        }


        [HttpGet]
        public ActionResult Update()
        {
            if (User.Identity.IsAuthenticated)
            {
                var model = new PacienteModel();
                var bllUtilidades = new BLL.Utilidades();
                var bllPaciente = new BLL.Paciente();
                var paciente = new BE.Paciente();
                string userId = User.Identity.GetUserId();
                var user = (new ApplicationDbContext()).Users.FirstOrDefault(s => s.Id == userId);
                paciente = bllPaciente.GetByUserName(user.UserName);
                model.Nombre = paciente.Nombre;
                model.Apellido = paciente.Apellido;
                model.Dni = paciente.Dni;
                model.ObrasSociales = bllUtilidades.GetObrasSociales().ToList();
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(PacienteModel paciente)
        {
            var bllPaciente = new BLL.Paciente();
            var bllUtilidades = new BLL.Utilidades();
            string userId = User.Identity.GetUserId();
            var user = (new ApplicationDbContext()).Users.FirstOrDefault(s => s.Id == userId);
            bllPaciente.Update(new BE.Paciente { Nombre = paciente.Nombre, Apellido = paciente.Apellido, Dni = paciente.Dni, NumeroAfiliado = paciente.NumeroAfiliado, ObraSocial = (BE.ObraSocial)Enum.Parse(typeof(BE.ObraSocial), paciente.ObraSocial), UserName = user.UserName });
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe.Value, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                    case SignInStatus.Failure:
                    default:
                        ViewBag.ErrorMessage = "The user name or password provided is incorrect";
                        return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new RegisterModel();
            var bll = new BLL.Utilidades();
            model.ObrasSociales = new List<string>();
            model.ObrasSociales = bll.GetObrasSociales().ToList();
            return View(model);
        }

        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                var bllPaciente = new BLL.Paciente();
                var bllUtilidades = new BLL.Utilidades();
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    await this.UserManager.AddToRoleAsync(user.Id, "Paciente");
                    bllPaciente.Add(new BE.Paciente { Nombre = model.Nombre, Apellido = model.Apellido, Dni = model.Dni, NumeroAfiliado = model.NumeroAfiliado, ObraSocial = (BE.ObraSocial)Enum.Parse(typeof(BE.ObraSocial), model.ObraSocial), UserName = model.UserName });
                    return RedirectToAction("Turnos", "Turnos");
                }
                model.ObrasSociales = bllUtilidades.GetObrasSociales().ToList();
                ViewBag.ErrorMessage = result.Errors.First();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect("/");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}