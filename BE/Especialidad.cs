﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public enum Especialidad
    {
        Pediatria = 1,
        Neumonologia = 2,
        Oftalmologia = 3,
        OtorrinoLaringologia = 4,
        Radiologia = 5,
        Traumatologia = 6,
        Kinesiologia = 7,
        MedicinaFamiliar = 8,
        Cardiologia = 9,
        Gastroenterologia = 10,
        Infectologia = 11,
        Reumatologia = 12, 
        Toxicologia = 13
    }
}
