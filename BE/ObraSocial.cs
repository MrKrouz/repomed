﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public enum ObraSocial
    {
        Osde = 1,
        Pami = 2,
        Galeno = 3,
        Ioma = 4,
        Swiss = 5
    }
}
