﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Turno
    {
        public Medico Profesional { get; set; }
        public Paciente Paciente { get; set; }
        public DateTime Fecha { get; set; }
        public int Id { get; set; }
    }
}
