﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Paciente : Usuario
    {
        public string Apellido { get; set; }

        public int Dni { get; set; }

        public int Id { get; set; }

        public string Nombre { get; set; }

        public string UserName { get; set; }


        public ObraSocial ObraSocial { get; set; }

        public long NumeroAfiliado { get; set; }
    }
}
