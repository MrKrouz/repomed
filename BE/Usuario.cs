﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public interface Usuario
    {
        int Id { get; set; }
        string UserName { get; set; }
        string Nombre { get; set; }
        string Apellido { get; set; }
        int Dni { get; set; }
    }
}
