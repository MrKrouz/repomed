﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Medico : Usuario
    {
        public string Apellido { get; set; }

        public int Dni { get; set; }

        public int Id { get; set; }

        public string Nombre { get; set; }

        public string UserName { get; set; }

        public Especialidad Especialidad { get; set; }

        public int Matricula { get; set; }
    }
}
