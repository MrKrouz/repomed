﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Turno
    {
        public void Add(BE.Turno Turno)
        {
            var dal = new DAL.Turno();
            var paciente = new Paciente();
            var medico = new Medico();
            Turno.Paciente = paciente.GetByName(Turno.Paciente.Nombre);
            Turno.Profesional = medico.GetById(Turno.Profesional.Id);
            dal.Add(Turno);
        }
        public void Update(BE.Turno Turno)
        {
            var dal = new DAL.Turno();
            var paciente = new Paciente();
            var medico = new Medico();
            Turno.Paciente = paciente.GetByName(Turno.Paciente.Nombre);
            Turno.Profesional = medico.GetById(Turno.Profesional.Id);
            dal.Update(Turno);
        }
        public void Remove(BE.Turno Turno)
        {
            var dal = new DAL.Turno();
            dal.Remove(Turno);
        }
        public BE.Turno GetByName(int idPaciente)
        {
            var dal = new DAL.Turno();
            return dal.GetByPaciente(idPaciente);
        }
        public BE.Turno GetById(int id)
        {
            var dal = new DAL.Turno();
            return dal.GetById(id);
        }
        public List<BE.Turno> GetTurnos()
        {
            var dal = new DAL.Turno();
            var paciente = new Paciente();
            var medico = new Medico();
            List<BE.Turno> turnos = new List<BE.Turno>();
            foreach (var item in dal.GetTurnos())
            {
                item.Profesional = medico.GetById(item.Profesional.Id);
                item.Paciente = paciente.GetById(item.Paciente.Id);
                turnos.Add(item);
            }
            return turnos;
        }
    }
}
