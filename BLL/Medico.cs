﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class Medico
    {
        public void Add(BE.Medico Medico)
        {
            var dal = new DAL.Medico();
            dal.Add(Medico);
        }
        public void Update(BE.Medico Medico)
        {
            var dal = new DAL.Medico();
            dal.Update(Medico);
        }
        public void Remove(BE.Medico Medico)
        {
            var dal = new DAL.Medico();
            dal.Remove(Medico);
        }
        public BE.Medico GetByName(string nombre)
        {
            var dal = new DAL.Medico();
            return dal.GetByName(nombre);
        }
        public BE.Medico GetById(int id)
        {
            var dal = new DAL.Medico();
            return dal.GetById(id);
        }
        public List<BE.Medico> GetMedicos()
        {
            var dal = new DAL.Medico();
            return dal.GetMedicos();
        }
    }
}
