﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class Paciente
    {
        public void Add(BE.Paciente paciente)
        {
            var dal = new DAL.Paciente();
            dal.Add(paciente);
        }
        public void Update(BE.Paciente paciente)
        {
            var dal = new DAL.Paciente();
            var be = new BE.Paciente();
            be = dal.GetByUserName(paciente.UserName);
            paciente.Nombre = be.Nombre;
            paciente.Apellido = be.Apellido;
            paciente.Id = be.Id;
            paciente.Dni = be.Dni;
            dal.Update(paciente);
        }
        public void Remove(BE.Paciente paciente)
        {
            var dal = new DAL.Paciente();
            dal.Remove(paciente);
        }
        public BE.Paciente GetByName(string nombre)
        {
            var dal = new DAL.Paciente();
            return dal.GetByName(nombre);
        }
        public BE.Paciente GetById(int id)
        {
            var dal = new DAL.Paciente();
            return dal.GetById(id);
        }
        public BE.Paciente GetByUserName(string userName)
        {
            var dal = new DAL.Paciente();
            return dal.GetByUserName(userName);
        }
        public List<BE.Paciente> GetPacientes()
        {
            var dal = new DAL.Paciente();
            return dal.GetPacientes();
        }
    }
}