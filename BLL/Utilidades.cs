﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class Utilidades
    {
        public IEnumerable<string> GetEspecialidades()
        {
            List<string> listaEspecialidades = new List<string>();
            foreach (var especialidad in Enum.GetNames(typeof(BE.Especialidad))) {
                listaEspecialidades.Add(especialidad);
            }
            return listaEspecialidades;
        }
        public IEnumerable<string> GetObrasSociales()
        {
            List<string> listaSociales = new List<string>();
            foreach (var obraSocial in Enum.GetNames(typeof(BE.ObraSocial)))
            {
                listaSociales.Add(obraSocial);
            }
            return listaSociales;
        }
    }
}
