﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Sql;
using BE;
namespace DAL
{
    public class Paciente
    {
        public void Add(BE.Paciente paciente)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "AltaPaciente", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Nombre", paciente.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", paciente.Apellido);
            cmd.Parameters.AddWithValue("@Dni", paciente.Dni);
            cmd.Parameters.AddWithValue("@NumeroAfiliado", paciente.NumeroAfiliado);
            cmd.Parameters.AddWithValue("@ObraSocial", (int)paciente.ObraSocial);
            cmd.Parameters.AddWithValue("@UserName", paciente.UserName);
            cmd.ExecuteNonQuery();
            cn.Close();

        }
        public void Update(BE.Paciente paciente)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "ModificarPaciente", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Nombre", paciente.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", paciente.Apellido);
            cmd.Parameters.AddWithValue("@Dni", paciente.Dni);
            cmd.Parameters.AddWithValue("@Id", paciente.Id);
            cmd.Parameters.AddWithValue("@NumeroAfiliado", paciente.NumeroAfiliado);
            cmd.Parameters.AddWithValue("@ObraSocial", (int)paciente.ObraSocial);
            cmd.Parameters.AddWithValue("@UserName", paciente.UserName);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void Remove(BE.Paciente paciente)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "BajaPaciente", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", paciente.Id);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public BE.Paciente GetByName(string nombre)
        {
            return GetPacientesDB().Where(m => m.Nombre == nombre).FirstOrDefault();
        }
        public BE.Paciente GetById(int id)
        {
            return GetPacientesDB().Where(m => m.Id == id).FirstOrDefault();
        }
        public BE.Paciente GetByUserName(string userName)
        {
            return GetPacientesDB().Where(m => m.UserName == userName).FirstOrDefault();
        }

        public List<BE.Paciente> GetPacientes()
        {
            return GetPacientesDB();
        }

        private List<BE.Paciente> GetPacientesDB()
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "GetPacientes", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            var listaPacientes = new List<BE.Paciente>();
            while (dr.Read())
            {
                listaPacientes.Add(new BE.Paciente { Nombre = dr.GetString(0), Apellido = dr.GetString(1), Dni = dr.GetInt32(2), ObraSocial = (BE.ObraSocial)dr.GetInt32(3), Id = dr.GetInt32(4), NumeroAfiliado = dr.GetInt64(5), UserName = dr.GetString(6) });
            }
            cn.Close();
            return listaPacientes;
        }
    }
}
