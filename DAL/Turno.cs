﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Turno
    {
        public void Add(BE.Turno turno)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "AltaTurno", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Fecha", turno.Fecha);
            cmd.Parameters.AddWithValue("@Paciente", turno.Paciente.Id);
            cmd.Parameters.AddWithValue("@Profesional", turno.Profesional.Id);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void Update(BE.Turno turno)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "ModificarTurno", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", turno.Id);
            cmd.Parameters.AddWithValue("@Fecha", turno.Fecha);
            cmd.Parameters.AddWithValue("@Paciente", turno.Paciente.Id);
            cmd.Parameters.AddWithValue("@Profesional", turno.Profesional.Id);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void Remove(BE.Turno turno)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "BajaTurno", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", turno.Id);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public BE.Turno GetById(int id)
        {
            return GetTurnosDB().Where(m => m.Id == id).FirstOrDefault();
        }
        public BE.Turno GetByPaciente(int idPaciente)
        {
            return GetTurnosDB().Where(m => m.Paciente.Id == idPaciente).FirstOrDefault();
        }

        public List<BE.Turno> GetTurnos()
        {
            return GetTurnosDB();
        }

        private List<BE.Turno> GetTurnosDB()
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "GetTurnos", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            var listaturnos = new List<BE.Turno>();
            while (dr.Read())
            {
                listaturnos.Add(new BE.Turno { Fecha = dr.GetDateTime(0), Paciente = new BE.Paciente { Id = dr.GetInt32(1) }, Profesional = new BE.Medico { Id = dr.GetInt32(2) } });
            }
            cn.Close();
            return listaturnos;
        }
    }
}
