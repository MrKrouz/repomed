﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Configuration;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class Medico
    {
        public void Add(BE.Medico Medico)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "AltaMedico", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Nombre", Medico.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", Medico.Apellido);
            cmd.Parameters.AddWithValue("@Dni", Medico.Dni);
            cmd.Parameters.AddWithValue("@UserName", Medico.UserName);
            cmd.Parameters.AddWithValue("@Matricula", Medico.Matricula);
            cmd.Parameters.AddWithValue("@Especialidad", (int)Medico.Especialidad);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void Update(BE.Medico Medico)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "ModificarMedico", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Nombre", Medico.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", Medico.Apellido);
            cmd.Parameters.AddWithValue("@UserName", Medico.UserName);
            cmd.Parameters.AddWithValue("@Id", Medico.Id);
            cmd.Parameters.AddWithValue("@Dni", Medico.Dni);
            cmd.Parameters.AddWithValue("@Matricula", Medico.Matricula);
            cmd.Parameters.AddWithValue("@Especialidad", (int)Medico.Especialidad);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void Remove(BE.Medico Medico)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "BajaMedico", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", Medico.Id);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public BE.Medico GetByName(string nombre)
        {
            return GetMedicosDB().Where(m => m.Nombre == nombre).FirstOrDefault();
        }
        public BE.Medico GetById(int id)
        {
            return GetMedicosDB().Where(m => m.Id == id).FirstOrDefault();
        }

        public List<BE.Medico> GetMedicos() {
            return GetMedicosDB();
        }

        private List<BE.Medico> GetMedicosDB()
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings["MedicureAP"];
            var cn = new SqlConnection(cs.ConnectionString);
            var cmd = new SqlCommand() { Connection = cn, CommandText = "GetMedicos", CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            var listaMedicos = new List<BE.Medico>();
            while (dr.Read())
            {
                listaMedicos.Add(new BE.Medico { Nombre = dr.GetString(0), Apellido = dr.GetString(1), Dni = dr.GetInt32(2), Especialidad = (BE.Especialidad)dr.GetInt32(3), Id = dr.GetInt32(4), Matricula = dr.GetInt32(5), UserName = dr.GetString(6) });
            }
            cn.Close();
            return listaMedicos;
        }
    }
}
